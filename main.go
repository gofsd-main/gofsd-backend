package main

import (
	"./server"
)

func main() {
	server.Create()
	server.Run()
}
