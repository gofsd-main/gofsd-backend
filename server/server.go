package server

import (
	"fmt"
	"net/http"

	"../store"
)

// Handler for testing
var Handler *http.ServeMux

// Run some comment
func Run() {

	http.ListenAndServe(":3000", Handler)
}

// Create some comment again
func Create() {
	Handler = http.NewServeMux()
	Handler.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, "Hello world %s", "Dima")
			break
		case http.MethodPost:
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, `{"foo":"bar"}`)
			break

		}
	})

	Handler.HandleFunc("/todos", func(w http.ResponseWriter, r *http.Request) {
		td := store.Todos{store.Todo{}}
		switch r.Method {
		case http.MethodGet:
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, td.All())
			break
		case http.MethodPost:
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, `{"foo":"bar"}`)
			break

		}
	})
}
