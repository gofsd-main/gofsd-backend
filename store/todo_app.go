package store

import (
	"encoding/json"
)

// Crud interface
type Crud interface {
	create(item interface{}) interface{}
	read(item interface{}) interface{}
	update(item interface{}) interface{}
	delete(item interface{}) interface{}
}

type Todo struct {
	ID        int
	Task      string
	Note      string
	Complete  bool
	Likes     int
	CreatorID string
}

type Todos []Todo

func structToReader(structure interface{}) string {
	mapA, _ := json.Marshal(structure)
	return string(mapA)
}

func (t *Todos) create(item Todo) string {
	item.ID = (*t)[len(*t)-1].ID + 1
	*t = append(*t, item)
	return structToReader(&item)
}

func (t Todos) read(ID int) string {
	for _, item := range t {
		if item.ID == ID {
			return structToReader(&item)
		}
	}
	return structToReader(&Todo{})
}

//All func
func (t Todos) All() string {
	return structToReader(&Todos1)
}

func (t Todos) update(ID int) Todo {
	for _, item := range t {
		if item.ID == ID {
			return item
		}
	}
	return Todo{}
}

func (t Todos) delete(ID int) Todo {
	for _, item := range t {
		if item.ID == ID {
			return item
		}
	}
	return Todo{}
}

// Todos1 array
var Todos1 Todos = Todos{Todo{}}
