package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"./server"
)

var (
	srv *httptest.Server
	url string
)

func TestMain(m *testing.M) {
	server.Create()
	srv = httptest.NewServer(server.Handler)
	url = fmt.Sprintf("%s/", srv.URL)
	defer srv.Close()
	m.Run()
}

func TestHandler(t *testing.T) {
	t.Run("Test get request", func(t *testing.T) {
		res, err := http.Get(url + "test")

		if err != nil {
			t.Fatal(err)
		}

		if res.StatusCode != http.StatusOK {
			t.Errorf("status not OK")
		}

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Fatal(err)
		}

		if string(body) != "Hello world Dima" {
			t.Fail()
		}
	})

	t.Run("Test post request", func(t *testing.T) {
		data := []byte(`{"foo":"bar"}`)
		r := bytes.NewReader(data)
		resp, err := http.Post(url+"test", "application/json", r)
		if err != nil {
			t.Error(err)
		}
		defer resp.Body.Close()

		if body, err := ioutil.ReadAll(resp.Body); string(body) != `{"foo":"bar"}` {
			if err != nil {
				t.Error(err)
			}
			t.Fail()
		}
	})
}
